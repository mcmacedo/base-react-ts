import React from "react";
import { createRoot } from "react-dom/client";

const App = () => <h1>My React and TypeScript App!</h1>

const container = document.getElementById("root");
// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(container!);
root.render(<App />);
